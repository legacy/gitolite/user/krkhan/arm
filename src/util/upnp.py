"""
Discovers and controls gateway routers via UPNP.
"""

import re
import socket
import sys
import time
import httplib
import urllib
import urllib2

from xml.etree.ElementTree import ElementTree

from util import log, torTools

SOAPXML = '''<?xml version="1.0"?>
<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
<s:Body>
<u:%(action)s xmlns:u="%(xmlns)s">
%(argData)s
</u:%(action)s>
</s:Body>
</s:Envelope>'''

def create_soap_xml(xmlns, action, args={}):
  argData = ''
  for key, value in args.items():
    argData += '<%s>%s</%s>\n' % (key, value, key)
  xml = SOAPXML % { 'xmlns' : xmlns, 'action' : action, 'argData' : argData }
  return xml

class UPnPError(Exception):
  def __init__(self, errorCode, errorDescription):
    self.errorCode = errorCode
    self.errorDescription = errorDescription
  def __str__(self):
    return 'UPnP Error (%s): %s' % (self.errorCode, self.errorDescription)

class UpnpDevice(object):
  def __init__(self, ip, response):
    self.ip = ip
    self.response = response
    self.name = None
    self.desc = {}
    self.control = {}

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    sock.connect(('18.0.0.1', 9))
    self.ifip, _ = sock.getsockname()

    conn = torTools.getConn()
    self.orport = conn.getOption('ORPort', 0)

    match = re.findall(r"LOCATION: (.*)", response)
    if not match:
      return
    location = match[0].strip()

    fd = None
    try:
      fd = urllib2.urlopen(location)
    except urllib2.URLError:
      self.name = self.ip
    else:
      self.parse_desc(fd)

      if 'manufacturer' in self.desc and 'UPC' in self.desc:
        self.name = "%s %s found at %s" % (self.desc['manufacturer'],
                                           self.desc['UPC'], self.ip)

      if 'url' in self.control:
        self.get_port_mappings()
        self.add_port_mapping()

  def parse_desc(self, fd):
    tree = ElementTree(file=fd)

    prefix = ''
    match = re.findall(r'({urn:.*})', tree.getroot().tag)
    if match:
      prefix = match[0]

    for elem in tree.getiterator():
      for key in ('URLBase', 'manufacturer', 'UPC'):
        if elem.tag == prefix + key:
          self.desc[key] = elem.text

      if elem.tag == prefix + 'service':
        service = dict([(e.tag.replace(prefix, ''), e.text) for e in elem.getchildren()])

        if 'WANIPConnection' in service['serviceType']:
          self.control['serviceType'] = service['serviceType']
          self.control['url'] = service['controlURL']

  def get_port_mappings(self):
    action = 'GetGenericPortMappingEntry'

    for i in range(3):
      args = { 'NewPortMappingIndex' : i }
      try:
        retvals = self.get_soap_response(action, args)
      except UPnPError, e:
        pass
      else:
        log.log(log.NOTICE, 'Port mapping found: ' + retvals['NewPortMappingDescription'])

  def add_port_mapping(self):
    if self.orport == '0':
      return

    action = 'AddPortMapping'
    args = {
      'NewRemoteHost'             : '',
      'NewExternalPort'           : self.orport,
      'NewProtocol'               : 'TCP',
      'NewInternalPort'           : self.orport,
      'NewInternalClient'         : self.ifip,
      'NewEnabled'                : '1',
      'NewPortMappingDescription' : 'Tor Relay',
      'NewLeaseDuration'          : '0',
    }

    try:
      self.get_soap_response(action, args)
    except UPnPError, e:
      pass
    else:
      log.log(log.NOTICE, 'Port mapping added for %s:%s' % (self.ifip, self.orport))

  def get_soap_response(self, action, args):
    headers = {
      'Content-Type' : 'text/xml',
      'SOAPAction': '"' + self.control['serviceType'] + '#' + action + '"',
    }
    xml = create_soap_xml(xmlns=self.control['serviceType'],
                          action=action,
                          args=args)

    conn = httplib.HTTPConnection(self.desc['URLBase'].replace('http://', ''))
    conn.request('POST', self.control['url'], xml, headers)
    response = conn.getresponse()

    tree = ElementTree(file=response)

    retvals = {}
    for elem in tree.getiterator():
      if 'Response' in elem.tag:
        retvals = dict([(c.tag, c.text) for c in elem.getchildren()])
      if 'UPnPError' in elem.tag:
        errorCode, errorDescription = -1, ''
        for c in elem.getchildren():
          if 'errorCode' in c.tag:
            errorCode = c.text
          if 'errorDescription' in c.tag:
            errorDescription = c.text
        raise UPnPError(errorCode, errorDescription)

    return retvals

